#ifdef GL_ES
precision mediump float;
#endif
uniform float u_time;
uniform vec2 u_resolution;

vec4 simpleAlphaCompositing (vec4 source, vec4 backdrop) {
	float final_alpha = source.a + backdrop.a * (1.0 - source.a);
	return vec4(
		(source.rgb * source.a + backdrop.rgb * backdrop.a * (1.0 - source.a)) / final_alpha,
		final_alpha
	);
}

void main( void ) {
        float time = u_time;
        vec2 resolution = u_resolution;
        vec2 uv = (gl_FragCoord.xy * 2. - resolution) / resolution.y;
	float tmp_x = uv.x;
	float tmp_y = uv.y;
	uv.x = tmp_y;
	uv.y = tmp_x;
	vec4 white = vec4(1.0, 1.0, 1.0, 1.0);
	vec4 background = vec4((16.0/255.0), (60.0/255.0), (72.0/255.0), 0.0);
	vec4 red_color = vec4((250.0/255.0), (87.0/255.0), (80.0/255.0), 0.0);
	vec4 green_color = vec4((117.0/255.0),(185.0/255.0),(56.0/255.0), 0.0);
	vec4 blue_color = vec4((70.0/255.0),(149.0/255.0),(247.0/255.0),0.0);
	// Control variables
	float scale_factor = 0.6;
	float squish_factor = 2.1;
	float time_factor = -0.2;
	float dim_factor = 0.3;
	// Relative travel speeds
	float red_speed = 1.0;
	float green_speed = 1.1;
	float blue_speed = 0.9;
	// Relative frequency
	float red_freq = 1.0;
	float green_freq = 1.1;
	float blue_freq = 0.9;
	// Relative amplitude
	float red_amp = 1.0;
	float green_amp = 1.4;
	float blue_amp = 0.6;
	// Get the y
	float y;
	// Do the red
	y = uv.y + (time * time_factor * red_speed);
	float red_d = uv.x - red_amp * scale_factor * sin(y * squish_factor * red_freq);
        float red = clamp((.01 / (red_d * red_d)) * dim_factor,0.0,1.0);
        red_color.a = red;
	background.a += red;
	// Do the green
	y = uv.y + (time * time_factor * green_speed);
	float green_d = uv.x - green_amp * scale_factor * sin(2.0944 + y * squish_factor * green_freq);
        float green = clamp((.01 / (green_d * green_d)) * dim_factor,0.0,1.0);
        green_color.a = green;
	background.a += green;
	// Do the blue
	y = uv.y + (time * time_factor * blue_speed);
	float blue_d = uv.x - blue_amp * scale_factor * sin(4.18879 + y * squish_factor * blue_freq);
        float blue = clamp((.01 / (blue_d * blue_d)) * dim_factor,0.0,1.0);
        blue_color.a = blue;
	background.a += blue;
	// Mix the colors
	background.a = 1.0 - clamp(background.a, 0.0, 1.0);
	vec4 color = simpleAlphaCompositing(red_color, vec4(0.0, 0.0, 0.0, 1.0));
	color = simpleAlphaCompositing(green_color, color);
	color = simpleAlphaCompositing(blue_color, color);
	vec4 result = simpleAlphaCompositing(background, color);
	gl_FragColor = result;
}
