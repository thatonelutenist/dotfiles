#extension GL_OES_standard_derivatives : enable

precision mediump float;

#define PI 3.14159265359

uniform float time;
uniform vec2 mouse;
uniform vec2 resolution;

float random (vec2 p) {
    return fract(sin(dot(p.xy, vec2(12.9898,78.233))) * 43758.5453123);
}

mat2 rotate2d(float angle){
	return mat2(cos(angle), -sin(angle), sin(angle), cos(angle));
}

void main( void ) {

	vec2 col = (gl_FragCoord.xy / resolution.xy);
	
	vec2 p = (gl_FragCoord.xy * 2.0 - resolution) / min(resolution.x, resolution.y);
	vec2 m = vec2(-(mouse.x * 4.0 - 2.0), -mouse.y * 2.0 + 1.0);

	vec3 color = vec3(0.0);
	for (float j = 0.0; j < 1.0; j += 0.1) {
		vec2 p3 = rotate2d((time / 3.0) + (j * PI / (1.0 + j))) * p;
		for (float i = 0.0; i < 3.5; i += 0.3) {
			vec2 p2 = p3;
			vec2 inf = vec2(0.0);
			inf.x += cos(time / 2.0 + i);
			inf.y += sin(time / 2.0 + i);
		
			inf.x = clamp(inf.x, -0.7, 0.7);
			inf.y = clamp(inf.y, -0.7, 0.7);
			
			p2.x += inf.x;
			p2.y += inf.y;
			
			p2 += random(p2) * p2;
			float h = (0.0002 + i / 1900000.0) / abs(length(p2));
			color += vec3(h);
		}
	}
	gl_FragColor = vec4(color * vec3(col.x, col.y, 1.0), 1.0);

}
