#!/usr/local/opt/python/libexec/bin/python
import subprocess
import pprint
import re
import datetime

## Fetch output from emacs

emacscommand = '/usr/local/bin/gccemacs -batch -l ~/.emacs.d/init.el -eval \
\'(org-batch-agenda "a" org-agenda-span (quote day) org-agenda-start-day "0d" org-agenda-skip-timestamp-if-done nil)\''

output = subprocess.run(['/usr/local/bin/fish','-c',emacscommand], capture_output=True, text=True).stdout

#print(output)

# Process the output
processed_output = {
    "untimed": [],
    "timed": [],
    "deadlines": [],
}

# Figure out where all the lines go
lines = output.splitlines()

for i in range(len(lines)):
    if i <= 1:
        continue
    line = lines[i].strip()
    line = re.sub(r'\s+',' ',line)
    if "---" not in line and "- - -" not in line:
        is_done = "DONE" in line
        # Deadlines (not today)
        if re.search(r'In\s*\d?\d\s*d.:',line) is not None:
            days = re.search(r'(\d?\d)\s*d',line).group(1)
            days = int(days)
            line = re.sub(r':\s*In\s*\d*\s*d\.?:\s*[A-Z]*\s',': ',line)
            processed_output['deadlines'].append((line, is_done, days))
        # Untimed Deadlines (today)
        elif re.search(r'Deadline',line) is not None and "..." not in line:
            line = re.sub(r':\s*Deadline:\s*[A-Z]*\s',': ',line)
            processed_output['deadlines'].append((line, is_done,0))
            continue
        # Timed deadlines (today)
        # Handle these like events
        elif 'Deadline' in line and "..." in line:
            date = re.search(r'\d?\d:\d\d',line).group(0)
            date = datetime.datetime.strptime(date,'%H:%M').time()
            line = re.sub(r'\.*\s',' ',line)
            line = re.sub(r'\s\d?\d:\d\d','',line)
            line = re.sub(r'Deadline:\s*[A-Z]*',"Deadline: ",line)
            processed_output["timed"].append((line,is_done,date))
        # Timed events
        elif "..." in line:
            date = re.search(r'\d?\d:\d\d',line).group(0)
            date = datetime.datetime.strptime(date,'%H:%M').time()
            processed_output["timed"].append((re.sub(r':\s?\d*:\d*\.*\s\S*',':',line),is_done,date))
        # Untimed events
        else:
            line = re.sub(r':\s*[A-Z]*\s',': ',line)
            processed_output["untimed"].append((re.sub(r':\S*',':',line),is_done))

# Sort timed entries by time of day

def time_sort(line):
    if line[1]:
        return 10000
    else:
        return line[2].hour * 60 + line[2].minute
processed_output["timed"].sort(key=time_sort)

# Sort untimed entries by completed
def complete_sort(line):
    if line[1]:
        return 2
    else:
        return 1
processed_output["untimed"].sort(key=complete_sort)

# Sort deadlines by days remaining
processed_output['deadlines'].sort(key=lambda a: a[2])

#pprint.pprint(processed_output)

## Print output

# Header
prefix = "🦄"
print(prefix)
print("---")

# associate a color with a time
def time_color(time):
    current_time = datetime.datetime.now().time()
    current_time = current_time.hour * 60 + current_time.minute
    time = time.hour * 60 + time.minute
    if current_time >= time:
        return "red"
    elif time - current_time <= 15:
        return "orange"
    elif time - current_time  < 60:
        return "yellow"
    else:
        return "white"
    
if len(processed_output["untimed"]) > 0:
    print("All Day: | color=green")
    for (line, done) in processed_output["untimed"]:
        if done:
            print("  *",line, "| color=#222222 trim=false")
        else:
            print("  *",line, "| color=white trim=false")
            
if len(processed_output["timed"]) > 0:
    print("Timed: | color=green")
    for (line,done,time) in processed_output["timed"]:
        if done:
            print("  *",line,"| color=#222222 trim=false")
        else:
            hour = time.hour
            if time.hour > 12:
                hour = hour - 12
            pod = "AM"
            if time.hour > 12:
                pod = "PM"
            print("  *",'{}:{:02d}{}'.format(hour, time.minute,pod),line,"| color={} trim=false".format(time_color(time)))

if len(processed_output['deadlines']) > 0:
    print("Deadlines: | color=green")
    values = set(map(lambda x:x[2], processed_output['deadlines']))
    all_deadlines = [(x,[y for y in processed_output['deadlines'] if y[2]==x]) for x in values]
    for (days, deadlines) in all_deadlines:
        if days == 0:
            print("  Today: | color=green trim=false")
        elif days == 1:
            print("  Tomorrow: | color=green trim=false")
        else:
            print("  In {} days: | color=green trim=false".format(days))
        for line in deadlines:
            print("    *",line[0],"| color=white trim=false")

