#!/bin/bash
set -x

# Reset
Normal='\033[0m'       # Text Reset

# Regular Colors
Black='\033[0;30m'        # Black
Red='\033[0;31m'          # Red
Green='\033[0;32m'        # Green
Yellow='\033[0;33m'       # Yellow
Blue='\033[0;34m'         # Blue
Purple='\033[0;35m'       # Purple
Cyan='\033[0;36m'         # Cyan
White='\033[0;37m'        # White

# Install a cargo package, for the first time
function install {
    echo -e "${Purple}Installing $1:${Normal}"
    cargo install "$@"
}

## RIIR of common shell utilites
install ripgrep --features pcre2
install fd-find

# Shell related
install starship

## General development related
install sccache --features all
install clog-cli
install fts_gitignore_nuke
install tokei
install wasm-pack

# Cargo plugins
install cargo-license
install cargo-bloat
install cargo-release
install cargo-workspaces
install cargo-edit
install cargo-asm
install cargo-audit
install cargo-license
install cargo-web
