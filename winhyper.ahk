#NoEnv ; recommended for performance and compatibility with future autohotkey releases.
#UseHook
#InstallKeybdHook
#SingleInstance force

SendMode Input

;; deactivate capslock completely
SetCapslockState, AlwaysOff

;; Remap CapsLock to control and vise versa
;;$Ctrl::CapsLock
;; $CapsLock::Ctrl 

;; note: must use tidle prefix to fire hotkey once it is pressed
;; not until the hotkey is released
~CapsLock::
    ;; must use downtemp to emulate hyper key, you cannot use down in this case 
    ;; according to https://autohotkey.com/docs/commands/Send.htm, downtemp is as same as down except for ctrl/alt/shift/win keys
    ;; in those cases, downtemp tells subsequent sends that the key is not permanently down, and may be 
    ;; released whenever a keystroke calls for it.
    ;; for example, Send {Ctrl Downtemp} followed later by Send {Left} would produce a normal {Left}
    ;; keystroke, not a Ctrl{Left} keystroke
    Send {AppsKey DownTemp}
    KeyWait, CapsLock
    Send {AppsKey Up}
    if (A_PriorKey = "CapsLock") {
        Send {Esc}
    }
return