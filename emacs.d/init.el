(let ((gc-cons-threshold most-positive-fixnum)
      (file-name-handler-alist nil))
  ;;;; Optimizations
  
  ;; A second, case-insensitive pass over `auto-mode-alist' is time wasted, and
  ;; indicates misconfiguration (don't rely on case insensitivity for file names).
  (setq auto-mode-case-fold nil)
  ;; Disabling the BPA makes redisplay faster, but might produce incorrect display
  ;; reordering of bidirectional text with embedded parentheses and other bracket
  ;; characters whose 'paired-bracket' Unicode property is non-nil.
  (setq bidi-inhibit-bpa t)  ; Emacs 27 only
  ;; Reduce rendering/line scan work for Emacs by not rendering cursors or regions
  ;; in non-focused windows.
  (setq-default cursor-in-non-selected-windows nil)
  (setq highlight-nonselected-windows nil)
  ;; More performant rapid scrolling over unfontified regions. May cause brief
  ;; spells of inaccurate syntax highlighting right after scrolling, which should
  ;; quickly self-correct.
  (setq fast-but-imprecise-scrolling t)
  ;; Don't ping things that look like domain names.
  (setq ffap-machine-p-known 'reject)
  ;; Resizing the Emacs frame can be a terribly expensive part of changing the
  ;; font. By inhibiting this, we halve startup times, particularly when we use
  ;; fonts that are larger than the system default (which would resize the frame).
  (setq frame-inhibit-implied-resize t)
  ;; Emacs "updates" its ui more often than it needs to, so we slow it down
  ;; slightly from 0.5s:
  (setq idle-update-delay 1.0)
  ;; Font compacting can be terribly expensive, especially for rendering icon
  ;; fonts on Windows. Whether disabling it has a notable affect on Linux and Mac
  ;; hasn't been determined, but we inhibit it there anyway. This increases memory
  ;; usage, however!
  (setq inhibit-compacting-font-caches t)
  ;; Introduced in Emacs HEAD (b2f8c9f), this inhibits fontification while
  ;; receiving input, which should help with performance while scrolling.
  (setq redisplay-skip-fontification-on-input t)
  ;; We want to compile all packages ahead of time
  (setq comp-deferred-compilation nil)
  
  ;; bootstrap straight.el
  (setq
   ;; Make a straight build dir based on the emacs version, to avoid trying to load bad bytecode
   straight-build-dir (format "build-%s" emacs-version)
   ;; Cache autoloads to reduce startup IO
   straight-cache-autoloads t
   ;; Enable package.el "integration"
   straight-enable-package-integration t
   ;; Save some bandwidth and disk space
   )
  (defvar bootstrap-version)
  (let ((bootstrap-file
         (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
        (bootstrap-version 5))
    (unless (file-exists-p bootstrap-file)
      (with-current-buffer
          (url-retrieve-synchronously
           "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
           'silent 'inhibit-cookies)
        (goto-char (point-max))
        (eval-print-last-sexp)))
    (load bootstrap-file nil 'nomessage))

  ;; Install use-package and setup integration
  (straight-use-package 'use-package)
  (setq use-package-always-defer t
        straight-use-package-by-default t)
  
  ;; Use latest Org
  (use-package org
    :demand t)
  (use-package org-contrib
    :demand t)
  (setq initial-major-mode 'org-mode)

  ;; This has to be in init.el because, well, stupid reasons
  (setq find-file-vist-truename t)
  (setq vc-follow-symlinks t)

  ;; Tangle configuration
  (org-babel-load-file (expand-file-name (concat user-emacs-directory "settings.org")))
  (garbage-collect))

