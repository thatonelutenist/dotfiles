(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(centaur-tabs-set-icons t)
 '(custom-enabled-themes '(sanityinc-solarized-dark))
 '(custom-safe-themes
   '("57a29645c35ae5ce1660d5987d3da5869b048477a7801ce7ab57bfb25ce12d3e" "4699fc4ecd6864097e1d0f7419405a00f978d82c8f97e6891d786d9ef9857262" "bee881414b9a82bd0aa015c55971ecdaf4e366bf814afdb18421861f37071899" "f0b0416502d80b1f21153df6f4dcb20614b9992cde4d5a5688053a271d0e8612" "d89e15a34261019eec9072575d8a924185c27d3da64899905f8548cbd9491a36" "833ddce3314a4e28411edf3c6efde468f6f2616fc31e17a62587d6a9255f4633" "aff12479ae941ea8e790abb1359c9bb21ab10acd15486e07e64e0e10d7fdab38" "4aee8551b53a43a883cb0b7f3255d6859d766b6c5e14bcb01bed572fcbef4328" "95bdf4c9c87618a377cd326794fd9d8fb3076ea3d5f4fe1f95a7d118e089ece2" "c433c87bd4b64b8ba9890e8ed64597ea0f8eb0396f4c9a9e01bd20a04d15d358" "2809bcb77ad21312897b541134981282dc455ccd7c14d74cc333b6e549b824f3" default))
 '(debug-on-error nil)
 '(fira-code-mode-disabled-ligatures '("x" "#["))
 '(lsp-ui-doc-include-signature t)
 '(lsp-ui-doc-use-webkit t)
 '(markdown-enable-math t)
 '(markdown-header-scaling nil)
 '(markdown-hide-markup t)
 '(neo-autorefresh t)
 '(neo-window-width 35)
 '(org-agenda-files
   '("/Users/nathan/Org/Calander.org" "/Users/nathan/Org/feeds.org"))
 '(package-selected-packages
   '(latex-preview-pane tide typescript projectile yasnippet-snippets nasm-mode ccls powershell elfeed-goodies elfeed-org elfeed magit-todos hl-todo org-brain pcre2el quelpa-use-package quelpa fish-mode load-theme-buffer-local color-theme-buffer-local emojify rust-mode htmlize theme-magic doom-modeline all-the-icons elcord-mode multiple-cursors deadgrep indent-guide edit-indirect markdownfmt smartparens window-purpose purpose auctex discover-my-major pdf-tools ox-gfm elcord i multi-term intero rustic dockerfile-mode yaml-mode yasnippet lsp-mode company flycheck org-ql avy f helm forge force vlf use-package toml-mode symon solarized-theme smooth-scrolling smex restclient rainbow-delimiters py-autopep8 powerline paredit ox-pandoc ox-clip org-wc org-sidebar org-plus-contrib org-chef org-bullets neotree mode-icons magit lsp-ui helm-org-rifle haskell-mode god-mode git-timemachine flycheck-rust eyebrowse exwm elpy dimmer diminish company-lsp color-identifiers-mode auto-package-update ahk-mode ace-window))
 '(tab-line-close-button-show 'selected)
 '(warning-suppress-log-types '((bytecomp))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "FiraCode Nerd Font" :width normal :height 120))))
 '(fixed-pitch ((t (:family "FiraCode Nerd Font" :width normal :height 120))))
 '(mode-line ((t (:inherit default :underline nil))))
 '(mode-line-inactive ((t (:inherit default :underline nil))))
 '(org-block ((t (:inherit fixed-pitch))))
 '(org-code ((t (:inherit (shadow fixed-pitch)))))
 '(org-document-info-keyword ((t (:inherit (shadow fixed-pitch)))))
 '(org-hide ((t (:inherit (shadow fixed-pitch)))))
 '(org-property-value ((t (:inherit fixed-pitch))) t)
 '(org-special-keyword ((t (:inherit (font-lock-comment-face fixed-pitch)))))
 '(org-table ((t (:inherit (shadow fixed-pitch)))))
 '(org-tag ((t (:inherit (shadow fixed-pitch) :weight bold))))
 '(org-verbatim ((t (:inherit (shadow fixed-pitch)))))
 '(tab-line ((t (:inherit variable-pitch :background "#002b36" :foreground "#839496" :height 0.9))))
 '(tab-line-tab-current ((t (:inherit tab-line-tab :background "#073642"))))
 '(tab-line-tab-inactive ((t (:inherit tab-line-tab :background "#002b36"))))
 '(variable-pitch ((t (:family "Roboto" :height 140)))))
