(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("c433c87bd4b64b8ba9890e8ed64597ea0f8eb0396f4c9a9e01bd20a04d15d358" "2809bcb77ad21312897b541134981282dc455ccd7c14d74cc333b6e549b824f3" default))
 '(debug-on-error nil)
 '(fira-code-mode-disabled-ligatures '("x" "#["))
 '(lsp-ui-doc-include-signature t)
 '(lsp-ui-doc-use-webkit t)
 '(mu4e-headers-fields
   '((:human-date . 8)
     (:flags . 6)
     (:to . 22)
     (:from . 22)
     (:thread-subject . 80)
     (:size . 7)))
 '(org-agenda-files '("~/Org/RepeatingTasks.org"))
 '(package-selected-packages
   '(## projectile org-brain fira-code-mode unicode-fonts evil-visual-mark-mode tide typescript pcre2el frame-purpose rainbow-identifiers esxml tracking a request anaphora matrix-client quelpa-use-package quelpa yasnippet-snippets org-contacts elfeed-goodies elfeed-org elfeed mu4e-alert latex-preview-pane magit-todos magit-todo hl-todo fish-mode load-theme-buffer-local color-theme-buffer-local emojify rust-mode htmlize theme-magic doom-modeline all-the-icons elcord-mode multiple-cursors deadgrep indent-guide edit-indirect markdownfmt smartparens window-purpose purpose auctex discover-my-major pdf-tools ox-gfm elcord i multi-term intero rustic dockerfile-mode yaml-mode yasnippet lsp-mode company flycheck org-ql avy f helm forge force vlf use-package toml-mode symon solarized-theme smooth-scrolling smex restclient rainbow-delimiters py-autopep8 powerline paredit ox-pandoc ox-clip org-wc org-sidebar org-plus-contrib org-chef org-bullets neotree mode-icons magit lsp-ui helm-org-rifle haskell-mode god-mode git-timemachine flycheck-rust eyebrowse exwm elpy dimmer diminish company-lsp color-identifiers-mode auto-package-update ahk-mode ace-window))
 '(tab-line-close-button-show nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "FiraCode" :width normal :height 90))))
 '(fixed-pitch ((t (:family "FiraCode" :width normal :height 90))))
 '(italic ((t (:underline nil :slant italic :family "Roboto"))))
 '(org-block ((t (:inherit fixed-pitch))))
 '(org-code ((t (:inherit (shadow fixed-pitch)))))
 '(org-document-info-keyword ((t (:inherit (shadow fixed-pitch)))))
 '(org-hide ((t (:inherit (shadow fixed-pitch)))))
 '(org-property-value ((t (:inherit fixed-pitch))) t)
 '(org-special-keyword ((t (:inherit (font-lock-comment-face fixed-pitch)))))
 '(org-table ((t (:inherit (shadow fixed-pitch)))))
 '(org-tag ((t (:inherit (shadow fixed-pitch) :weight bold))))
 '(org-verbatim ((t (:inherit (shadow fixed-pitch)))))
 '(tab-line ((t (:inherit variable-pitch :background "#002b36" :foreground "#839496" :height 0.9))))
 '(tab-line-tab-current ((t (:inherit tab-line-tab :background "#073642"))))
 '(tab-line-tab-inactive ((t (:inherit tab-line-tab :background "#002b36"))))
 '(variable-pitch ((t (:family "Roboto" :height 105)))))
