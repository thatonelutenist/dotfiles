#!/bin/sh
set -euo pipefail

# Reset
Normal='\033[0m'       # Text Reset

# Regular Colors
Black='\033[0;30m'        # Black
Red='\033[0;31m'          # Red
Green='\033[0;32m'        # Green
Yellow='\033[0;33m'       # Yellow
Blue='\033[0;34m'         # Blue
Purple='\033[0;35m'       # Purple
Cyan='\033[0;36m'         # Cyan
White='\033[0;37m'        # White

function section {
    echo -e "${Purple}Configuring $1:${Normal}"
}

# Link a file
function link_file {
    root=$(dirname "$2")
    mkdir -p "$root"
    echo "  Linking file $1 to $2"
    if [[ -f "$2" ]]; then
        echo -e "    ${Blue}Success:${Normal} File already linked"
    else
        ln -s "$(pwd)/$1" "$2"
        echo -e "    ${Green}Success:${Normal} File now linked"
    fi   
}

# Link a directory
function link_dir {
    mkdir -p $(dirname $2)
    echo "  Linking directory $1 to $2"
    if [[ -d "$2" ]]; then
        echo -e "    ${Blue}Success:${Normal} Directory already linked"
    else
        ln -s "$(pwd)/$1" $2
        echo -e "    ${Green}Success:${Normal} Directory now linked"
    fi  
}

# set the machine type
unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     machine="linux";;
    Darwin*)    machine="mac";;
    *)          machine="UNKNOWN:${unameOut}"
esac

section "X11"
link_file xsession ~/.xsession
link_file xmodmap ~/.Xmodmap
link_file xprofile ~/.xprofile

section "Sway"
link_file sway/config ~/.config/sway/config

section "Waybar"
link_file waybar/config.json ~/.config/waybar/config
link_file waybar/style.css ~/.config/waybar/style.css

section "Emacs"
link_file emacs.d/early-init.el ~/.emacs.d/early-init.el
link_file emacs.d/init.el ~/.emacs.d/init.el
link_file emacs.d/settings.org ~/.emacs.d/settings.org
link_dir emacs.d/site-lisp ~/.emacs.d/site-lisp
link_dir emacs.d/versions ~/.emacs.d/straight/versions
link_dir emacs.d/custom ~/.emacs.d/custom
link_dir emacs.d/snippets ~/.emacs.d/snippets

section "SSH"
link_file ssh_config ~/.ssh/config

section "Fish"
link_file fish/config.fish ~/.config/fish/config.fish

section "Git"
link_file gitignore ~/.gitignore_global
link_file gitconfig ~/.gitconfig 

section "Starship"
link_file starship.toml ~/.config/starship.toml

section "Element"
if [ "$machine" = "mac" ]; then
    element_config_location="$HOME/Library/Application Support/Element/config.json"
else
    element_config_location="$HOME/.config/Element/config.json"
fi
link_file Element/config.json "$element_config_location"

section "Alacritty"
link_file alacritty.yml ~/.config/alacritty/alacritty.yml


echo -e "${Green}Success:${Normal} Configuration has now been bootstrapped"
