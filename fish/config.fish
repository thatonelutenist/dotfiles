### Environment variables

# check to see if we are inside emacs, if so, load the vterm fish file
if set -q INSIDE_EMACS
    source ~/.emacs.d/straight/repos/emacs-libvterm/etc/emacs-vterm.fish
end

# Alacritty rename hooks
function rename_directory --on-event fish_prompt
    if set -q ALACRITTY
        echo -ne "\e]2;fish: "(dirs)"\007"
    end
end

function rename_command --on-event fish_preexec
    if set -q ALACRITTY
        set -l command (echo $argv[1] | cut -d ' ' -f 1)
        echo -ne "\e]2;("(dirs)"): $command\007"
    end
end

# setup gpg
set -x GPG_TTY (tty)
gpg-connect-agent /bye
set -x SSH_AUTH_SOCK /run/user/1000/gnupg/S.gpg-agent.ssh


## Path
set -gx PATH ~/.local/bin ~/.cargo/bin ~/Scripts/ ~/.cabal/bin ~/.ghcup/bin /usr/local/MacGPG2/bin $PATH
# use gnuutils on macos
if test -e ~/.linuxify
    fenv source ~/.linuxify
end
# use nix-shell if setup
if test -e ~/.nix-profile/etc/profile.d/nix.sh
    fenv source ~/.nix-profile/etc/profile.d/nix.sh
end
if type -q any-nix-shell
    any-nix-shell fish --info-right | source
end

# Set LIBRARY_PATH on osx
if [ (uname) = "Darwin" ]
    set -gx LIBRARY_PATH "$LIBRARY_PATH:/usr/local/lib"
    set -gx CMAKE_PREFIX_PATH /usr/local/opt/icu4c
end

## Wasmtime
set -gx WASMTIME_HOME "$HOME/.wasmtime"
string match -r ".wasmtime" "$PATH" > /dev/null; or set -gx PATH "$WASMTIME_HOME/bin" $PATH

## Icontheme for albert on linux
set -gx ALBERT_ICON_THEME "candy-icons"

# detect current emacs
if type -q gccemacs
    set emacs "gccemacs"
    set emacsclient "gccemacsclient"
else
    set emacs "emacs"
    set emacsclient "emacsclient"
end

## EDITOR and VISUAL
if set -q SSH_CLIENT
    set -gx EDITOR "zile"
    set -gx VISUAL "zile"
else
    set -gx EDITOR "$emacsclient --alternate-editor \"\""
    set -gx VISUAL "$emacsclient --alternate-editor \"\""
end

### Logs

# Setup command logging to ~/.logs
function cmdlogger --on-event fish_preexec
	mkdir -p ~/.logs
	echo (date -u +"%Y-%m-%dT%H:%M:%SZ")" "(echo %self)" "(pwd)": "$argv >> ~/.logs/(hostname)-(date "+%Y-%m-%d").log
end

### General setup

# Supress title bar changes
function fish_title;end

# setup starship, if configured
if type -q starship
   starship init fish | source
end


### Aliases

## Emacs
alias ecn "$emacsclient -nc --alternate-editor \"\""
alias ec "$emacsclient --alternate-editor \"\""

## Shell commands

# Replace ls with lsd 
if type -q exa
    alias ls "exa --icons"
end

# Replace cat with bat
if type -q bat
    alias cat bat
end

### Jenv
if type -q jenv
    status --is-interactive; and source (jenv init -|psub)
end

### GPG
set -x GPG_TTY (tty)
if test -e ~/.gpg-agent-info
	fenv source ~/.gpg-agent-info
	set -x SSH_AUTH_SOCK (gpgconf --list-dirs agent-ssh-socket)
	gpgconf --launch gpg-agent
end

